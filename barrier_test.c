#include "types.h"
#include "user.h"

const int N = 5;

int main(void)
{
    int mainPid = getpid();
    if(barrier_init(N + 1) < 0)
    {
        printf(2, "barrier init failed\n");
        exit();
    }

    barrier_init(N + 1);

    for (int i = 0; i < N; i++)
    {
        int pid = getpid();
        if (mainPid == pid)
        {
            fork();
            delay(1);
        }
    }

    delay(getpid());
    barrier_wait();

    while(wait() > 0)
        ;

    if (mainPid == getpid())
    {
        barrier_destroy();
    }
    exit();
}
