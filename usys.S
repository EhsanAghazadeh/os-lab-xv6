#include "syscall.h"
#include "traps.h"

#define SYSCALL(name) \
  .globl name; \
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
SYSCALL(exit)
SYSCALL(wait)
SYSCALL(pipe)
SYSCALL(read)
SYSCALL(kill)
SYSCALL(exec)
SYSCALL(fstat)
SYSCALL(chdir)
SYSCALL(dup)
SYSCALL(getpid)
SYSCALL(sbrk)
SYSCALL(sleep)
SYSCALL(uptime)
SYSCALL(open)
SYSCALL(write)
SYSCALL(mknod)
SYSCALL(unlink)
SYSCALL(link)
SYSCALL(mkdir)
SYSCALL(close)
SYSCALL(count_num_of_digits)
SYSCALL(set_path)
SYSCALL(get_parent_id)
SYSCALL(get_children)
SYSCALL(delay)
SYSCALL(get_time)
SYSCALL(print_all_process_info)
SYSCALL(change_queue)
SYSCALL(change_priority)
SYSCALL(change_ticket_number)
SYSCALL(test_reentrant_acquire)
SYSCALL(barrier_init)
SYSCALL(barrier_wait)
SYSCALL(barrier_destroy)
