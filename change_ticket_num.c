#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char **argv)
{
    if(argc < 3)
    {
        printf(2, "usage: change_ticket_num pid new_tickets_num\n");
        exit();
    }

    return -1 * change_ticket_number(atoi(argv[1]), atoi(argv[2]));
}
