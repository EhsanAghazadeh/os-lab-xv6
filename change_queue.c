#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char **argv)
{
    if(argc < 3)
    {
        printf(2, "usage: change_queue pid new_queue\n");
        exit();
    }

    return -1 * change_queue(atoi(argv[1]), atoi(argv[2]));
}
